import { PhysicsBody } from './PhysicsBody'
import { Vector2 } from './Vector2'

export class State {
  bodies: PhysicsBody[] = []
  constraintPos: Vector2 = new Vector2(100, 100)
  gravity = new Vector2(0, 1000)
  constraintRadius: number = 100
  mouseX = 0
  mouseY = 0
  size = 10
  showSpawner = true
  mouseMoved = false

  solverRate = 0
  framerate = 0
}
