import { PhysicsBody } from './PhysicsBody'
import { Renderer } from './Renderer'
import { Solver } from './Solver'
import { State } from './State'
import { updateDebug } from './updateDebug'
import { Vector2 } from './Vector2'

const canvas = document.getElementById('canvas') as HTMLCanvasElement
const state = new State()

const solver = new Solver(state)
const renderer = new Renderer(state, canvas)

let shiftDown = false

const calculateConstraints = () => {
  const bounds = canvas.getBoundingClientRect()
  canvas.width = bounds.width
  canvas.height = bounds.height
  
  const center = new Vector2(canvas.width / 2, canvas.height / 2)
  
  state.constraintPos = center
  state.constraintRadius = Math.min(canvas.width, canvas.height) / 2
}

let time = Date.now()
let lotsSpawned = false
const SPAWN_COUNT = 500

const spawnLots = () => {
  if (lotsSpawned) return
  lotsSpawned = true
  for (let i = 0; i < SPAWN_COUNT; i++) {
    const dist = Math.random() * state.constraintRadius
    const rad = Math.random() * 2 * Math.PI
    const x = Math.sin(rad) * dist + canvas.width / 2
    const y = Math.cos(rad) * dist + canvas.height / 2
    createBody(x, y, 5)
  }
}

const calculateFramerate = (frames: number[], maxFrames: number) => {
  if (frames.length >= maxFrames) {
    frames.splice(0, frames.length - maxFrames + 1)
  }
  frames.push(Date.now())

  return 1000 * frames.length / (frames[frames.length - 1] - frames[0])
}

const solverFrames: number[] = []
const SOLVER_FRAME_COUNT = 30

setInterval(() => {
  state.solverRate = calculateFramerate(solverFrames, SOLVER_FRAME_COUNT)
  const newTime = Date.now()
  const dt = (newTime - time) / 1000
  time = newTime

  solver.update(dt)
}, 0)

const renderFrames: number[] = []
const RENDER_FRAME_COUNT = 30

const render = () => {
  state.framerate = calculateFramerate(renderFrames, RENDER_FRAME_COUNT)
  if (shiftDown) {
    const scale = Math.random()
    state.size = scale * scale * (MAX_SIZE - MIN_SIZE) + MIN_SIZE
  }
  renderer.render()
  updateDebug(state)
  requestAnimationFrame(render)
}

calculateConstraints()
render()

state.mouseX = canvas.width / 2
state.mouseY = canvas.height / 2

canvas.addEventListener('mousemove', (ev) => {
  state.mouseX = ev.offsetX
  state.mouseY = ev.offsetY
  state.mouseMoved = true
})

canvas.addEventListener('click', (ev) => {
  createBody(ev.offsetX, ev.offsetY)
})

window.addEventListener('focus', () => {
  time = Date.now()
})

window.addEventListener('resize', () => {
  calculateConstraints()
})

const MIN_SIZE = 5
const MAX_SIZE = 25

function randomColor() {
  return `hsl(${Math.random() * 360}deg 100% 50%)`
}

const createBody = (x: number, y: number, size = state.size) => {
  state.bodies.push(new PhysicsBody(new Vector2(x, y), size, size * size, 0, randomColor()))
}
const createAtMouse = () => {
  createBody(state.mouseX, state.mouseY)
}
let spamInterval: any = null

window.addEventListener('keydown', (ev) => {
  if (ev.key === ' ' && !spamInterval) {
    createAtMouse()
    spamInterval = setInterval(createAtMouse, 333)
  } else if (ev.key === 's') {
    spawnLots()
  } else if (ev.shiftKey) {
    shiftDown = true
  }
})

window.addEventListener('keyup', (ev) => {
  if (ev.key === ' ') {
    clearInterval(spamInterval)
    spamInterval = null
  } else if (!ev.shiftKey) {
    shiftDown = false
  }
})

document.addEventListener('wheel', (ev) => {
  state.size = Math.max(MIN_SIZE, Math.min(state.size + Math.sign(ev.deltaY), MAX_SIZE))
})

document.addEventListener('touchstart', (ev) => {
  state.showSpawner = false
})

if (typeof Accelerometer === "function") {
  const startAccelerometer = () => {
    const accel = new Accelerometer({ frequency: 60 })
    accel.addEventListener('reading', () => {
      if (accel.x === undefined || accel.y === undefined) return

      state.gravity = new Vector2(accel.x * -100, accel.y * 100)
    })
    accel.start()
  }

  navigator.permissions.query({ name: 'accelerometer' as any }).then((result) => {
    if (result.state === 'granted') {
      startAccelerometer()
    }

    result.addEventListener('change', () => {
      if (result.state === 'granted') {
        startAccelerometer()
      }
    })
  })
}
