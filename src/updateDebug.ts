
import { State } from './State'

export function updateDebug (state: State) {
  const debugEl = document.getElementById('debug')!
  debugEl.innerHTML = `
    <p>Gravity X: ${Math.floor(state.gravity.x)}</p>
    <p>Gravity Y: ${Math.floor(state.gravity.y)}</p>
    <p>Balls: ${state.bodies.length}</p>
    <p>Solver rate: ${Math.floor(state.solverRate)}</p>
    <p>Framerate: ${Math.floor(state.framerate)}</p>
  `
}
