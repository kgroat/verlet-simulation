
export class Vector2 {
  constructor (
    public readonly x: number,
    public readonly y: number,
  ) {}

  add(b: Vector2) {
    return new Vector2(this.x + b.x, this.y + b.y)
  }

  subtract(b: Vector2) {
    return new Vector2(this.x - b.x, this.y - b.y)
  }

  multiply(scale: number) {
    return new Vector2(this.x * scale, this.y * scale)
  }

  normalize() {
    return this.multiply(1 / this.length)
  }

  get length() {
    return Math.hypot(this.x, this.y)
  }

  static zero = new Vector2(0, 0)
}
