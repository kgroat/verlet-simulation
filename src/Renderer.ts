
import { State } from './State'

export class Renderer {
  constructor(
    private state: State,
    private canvas: HTMLCanvasElement
  ) {}

  render() {
    const { constraintPos, constraintRadius, bodies, mouseX, mouseY, size, showSpawner, mouseMoved } = this.state
    const ctx = this.canvas.getContext('2d')
    if (!ctx) {
      return
    }
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)

    ctx.fillStyle = '#888'
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)

    ctx.beginPath()
    ctx.arc(constraintPos.x, constraintPos.y, constraintRadius, 0, 2 * Math.PI)
    ctx.fillStyle = '#fff'
    ctx.fill()

    bodies.forEach(b => {
      ctx.beginPath()
      ctx.arc(b.currPosition.x, b.currPosition.y, b.radius, 0, 2 * Math.PI)
      ctx.fillStyle = b.color
      ctx.fill()
    })

    if (showSpawner && mouseMoved) {
      ctx.beginPath()
      ctx.arc(mouseX, mouseY, size, 0, 2 * Math.PI)
      ctx.fillStyle = 'rgb(255 0 0 / 25%)'
      ctx.fill()
    }
  }
}
