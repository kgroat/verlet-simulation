
import { State } from './State'

export class Solver {
  subSteps = 1
  constructor(
    private state: State
  ) {}

  update(dt: number) {
    const subDt = dt / this.subSteps

    for (let i = 0; i < this.subSteps; i++) {
      this.applyGravity()
      this.applyConstraint()
      this.solveCollisions()
      this.updatePositions(subDt)
    }
  }

  private updatePositions(dt: number) {
    this.state.bodies.forEach(b => b.update(dt))
  }

  private applyGravity() {
    this.state.bodies.forEach(b => b.accelerate(this.state.gravity))
  }

  private applyConstraint() {
    const { constraintPos, constraintRadius, bodies } = this.state
    bodies.forEach(body => {
      const toObj = body.currPosition.subtract(constraintPos)
      const dist = toObj.length

      if (dist > constraintRadius - body.radius) {
        const diff = toObj.normalize().multiply(constraintRadius - body.radius)
        const newPosition = constraintPos.add(diff)
        const delta = newPosition.subtract(body.currPosition)
        body.currPosition = body.currPosition.add(delta.multiply(1 + body.bounciness))
      }
    })
  }

  private solveCollisions() {
    const { bodies } = this.state
    for (let i = 0; i < bodies.length; i++) {
      const one = bodies[i]
      for (let j = i + 1; j < bodies.length; j++) {
        const two = bodies[j]
        const collisionAxis = one.currPosition.subtract(two.currPosition)
        const dist = collisionAxis.length
        if (dist < one.radius + two.radius) {
          const delta = one.radius + two.radius - dist
          const bounciness = 1 + (one.bounciness + two.bounciness) / 2
          const diff = collisionAxis.normalize().multiply(delta * bounciness)
          const massDiff = two.mass / (one.mass + two.mass)

          one.currPosition = one.currPosition.add(diff.multiply(massDiff))
          two.currPosition = two.currPosition.subtract(diff.multiply(1 - massDiff))
        }
      }
    }
  }
}
