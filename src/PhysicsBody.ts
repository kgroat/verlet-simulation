
import { Vector2 } from './Vector2'

export class PhysicsBody {
  prevPosition: Vector2
  acceleration: Vector2

  constructor(
    public currPosition: Vector2,
    public radius = 10,
    public mass = 10,
    public bounciness = 0.25,
    public color = '#00f'
  ) {
    this.prevPosition = currPosition
    this.acceleration = Vector2.zero
  }

  update(dt: number) {
    // Get current velocity
    const velocity = this.currPosition.subtract(this.prevPosition)
    this.prevPosition = this.currPosition
    // Perform verlet
    this.currPosition = this.currPosition.add(velocity).add(this.acceleration.multiply(dt * dt))
    // Reset acceleration
    this.acceleration = Vector2.zero
  }

  accelerate(acc: Vector2) {
    this.acceleration = this.acceleration.add(acc)
  }
}
