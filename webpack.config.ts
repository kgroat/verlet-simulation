
import HtmlWebpackPlugin from 'html-webpack-plugin'
import { join } from 'path'
import { Configuration } from 'webpack'
import 'webpack-dev-server'

const config = ({ dev }: { dev: boolean }): Configuration => {
  return {
    entry: join(__dirname, 'src/index.ts'),
    devServer: {
      host: '0.0.0.0',
      port: 3000,
      static: 'pub'
    },
    output: {
      path: join(__dirname, 'public'),
      publicPath: dev ? '/' : '/verlet-simulation/'
    },
    resolve: {
      extensions: [
        '.ts',
        '.tsx',
        '.js'
      ],
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'ts-loader',
            },
          ],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        inject: 'body',
        template: join(__dirname, 'index.html'),
      }),
    ],
    mode: dev ? 'development' : 'production',
    devtool: 'eval-cheap-module-source-map'
  }
}

export default config
